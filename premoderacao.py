import pickle
import numpy as np

class PreModeracao():
    def __init__(self):
        self.loaded_pipeline = self._load_model()

    def _load_model(self):
        """Loads the pikle model and returns ready to use."""
        file_path = "models/modelo_odio_decidim.pkl"
        with open(file_path, "rb") as f:
            loaded_pipeline = pickle.load(f)
        return loaded_pipeline

    def isoffencive(self, input_string: str) -> dict:
        """Applies the pikle model on text

        Args:
            input_string: Text to predict.

        Returns:
            A dict with information of pre moderate text.
        """
        scores_ = self.loaded_pipeline.predict_proba([input_string])
        max_class_ = np.argmax(scores_)

        map_label = {   '1': 'antisemitism',
                        '2': 'apology for the dictatorship',
                        '3': 'fatphobia',
                        '4': 'homophobia',
                        '5': 'partyism',
                        '6': 'racism',
                        '7': 'religious intolerance',
                        '8': 'sexism',
                        '9': 'xenophobia',
                        '-1': 'offensive & non-hate speech',
                        '0': 'non-offensive'
                    }
        
        classes = ['-1', '0', '1', '2', '2,5', '3', '3,5', '3,8', '4', '4,6', '5', '5,8', '6', '7', '8', '9']
        result = {"offensive": classes[max_class_],
                "offense type": map_label[classes[max_class_]],
                }
        return result
