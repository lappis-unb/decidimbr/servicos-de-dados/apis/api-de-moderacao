# API De Moderação

API feita utilizando o [FastAPI](https://fastapi.tiangolo.com/) para consultar o modelo de pré moderação, passando um texto ou uma lista de textos para ser julgado como ofensivo ou não.

## Getting started

Para instalar crie uma virtualenv e instale as dependências:
 ```
    $ pip install fastapi
    $ pip install "uvicorn[standard]"
 ```

Para rodar o servidor:
```
    $ uvicorn main:app --reload
```

Para testar o modelo com um texto por vez acesse a api pelo navegador no endereço http://127.0.0.1:8000/text/?text=\<adicione o texto a ser moderado aqui>

Para testar uma lista de texto acesse a api pelo navegador no endereço http://127.0.0.1:8000/items/?text=\<adicione o primeiro texto a ser moderado aqui>&text=\<adicione o segundo texto a ser moderado aqui>
