from fastapi import FastAPI, Query
from typing import List, Union

from premoderacao import PreModeracao

app = FastAPI()

premoderacao = PreModeracao()
results = []


@app.get("/text/")
def read_text(text: str):
    return premoderacao.isoffencive(text)

@app.get("/items/")
async def read_items(
    text: List[Union[str, None]] = Query(default=None),
):

    for t in text:
        result = premoderacao.isoffencive(t)
        results.append(result)
    return results